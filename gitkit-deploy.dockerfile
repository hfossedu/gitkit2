FROM alpine
RUN apk update && \
    apk add --no-cache bash git yq glab
WORKDIR /gitkit
COPY . ./
ENTRYPOINT ["./deploy.sh"]
