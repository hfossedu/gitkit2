#!/usr/bin/env bash

set -e

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";

export TARGET_GROUP="${1}"
export PROJ_DIR="${SCRIPT_DIR}"
export REPO_DIR="${SCRIPT_DIR}/repository"
export GIT_DIR="${SCRIPT_DIR}/repository/.git"
export KIT_DIR="${SCRIPT_DIR}/repository/.kit"

deploy() {
    clone
    create-remote
    install-features
    push
}

clone() {
    (
        mkdir -p "${REPO_DIR}"
        cd "${REPO_DIR}"
        git clone https://github.com/DickinsonCollege/FarmData2.git .

        # We must checkout each branch we want to keep.
        # We should pin them to a known commit so the kit is repeatable.
        git switch main
        git reset --hard d622e8d6d71e27890c73e2428e6dcf9d44ca606e
        git remote remove origin

        # To speed up push and clones, we squash commits since the first.
        git reset --soft "$(git rev-list --max-parents=0 --first-parent HEAD)"
        git add .
        git config user.email "kit@example.com"
        git config user.name "kit"
        git commit -m "chore(kit): squash history"
    )
}

create-remote() {
    (
        cd "${REPO_DIR}"
        local group="$(get-group-name "${TARGET_GROUP}")"
        local proj="$(get-project-name)"
        glab auth login --token "${GL_TOKEN}"
        glab repo create "${group}/${KIT_PROJECT_PREFIX}${proj}" --public --group ${group}
        git remote add origin "https://oauth2:${GL_TOKEN}@gitlab.com/${group}/${KIT_PROJECT_PREFIX}${proj}"
    )
}

get-group-name() {
    local n="$1"
    n="${n##*gitlab.com/}"
    n="${n%.git}"
    echo "$n"
}

get-project-name() {
    basename "${PROJ_DIR}"
}

install-features() {
    echo "install-features"
    (
        mkdir -p "${KIT_DIR}"
        cp -R "${PROJ_DIR}"/features "${KIT_DIR}"

        glab auth login --token "${GL_TOKEN}"
        git remote -v

        for f in "${KIT_DIR}"/features/* ; do
        (
            cd "${f}"
            test ! -e ./install-into-instance.sh || ./install-into-instance.sh
        )
        done

        (
            cd "${REPO_DIR}"
            git add .
            git commit -m "build(kit): install features"
        )
    )
}

push() {
    (
        cd "${REPO_DIR}"
        git push --all
        git push --tags
    )
}

deploy
